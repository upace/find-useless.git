import { parseArgs } from '../src/utils';
describe('test utils.parseArgs', () => {
  it('parseArgs', () => {
    expect(parseArgs(['--excludes="js|jsx"'])).toEqual({
      '--excludes': 'js|jsx',
    });
    expect(parseArgs(['--excludes="js|jsx"', '--alias="{"@":"src"}"'])).toEqual({
      '--alias': '{"@":"src"}',
      '--excludes': 'js|jsx',
    });
  });
});
