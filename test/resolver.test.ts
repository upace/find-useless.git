import path from 'path';
import { resolve } from '../src/resolver';
describe('test resolver', () => {
  it('存在时，返回绝对路径', async () => {
    expect(await resolve(process.cwd(), './package.json')).toBe(path.resolve(process.cwd(), 'package.json'));
  });
});
