# find-useless

查找前端项目中没有使用到的文件

![](./img/2022-03-25_18-20.png)

## 原理

1. 收集 git 仓库管理的文件
2. 排除通过 babel 编译的文件
3. 排除 babel 编译过程中依赖的文件

## 使用

1. 安装
   ```bash
   yarn add @upace/find-useless --dev
   ```
2. 配置 babel.config.js

   ```js
   const { injectBabelPlugin } = require('@upace/find-useless');
   // 不用担心性能，不执行 find-useless 时不会注入
   module.exports = injectBabelPlugin({
     // ...
   });
   ```

3. 执行构建命令
   ```bash
   # 假设你的构建命令为 yarn build
   yarn find-useless when yarn build
   yarn find-useless --includes=src --excludes='\.tsx|\.jsx' when yarn build
   ```
4. 分析 react-native
   ```bash
   yarn find-useless --includes=src --excludes='test|spec|\.md' when yarn react-native bundle --dev false --platform android --entry-file index.tsx --bundle-output index.android.bundle --reset-cache
   ```
