import http from 'http';
import { EventEmitter } from 'events';

class Server extends EventEmitter {
  httpServer = http.createServer((req, res) => {
    const url = new URL(req.url!, `http://${req.headers.host}`);
    const file = url.searchParams.get('file');
    this.emit('report-file', file);
    res.end('');
  });

  async listen(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.httpServer.listen(process.env.FIND_USELESS_PORT, resolve).on('error', reject);
    });
  }
}
export default new Server();
