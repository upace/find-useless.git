import { reportFile } from './utils';
import type { Visitor } from '@babel/core';
import path from 'path';
import { resolve } from './resolver';

async function resolveAndReport(filepath: string, moduleName: string) {
  const fullpath = await resolve(path.dirname(filepath), moduleName);
  if (fullpath) {
    reportFile(fullpath);
  }
}

export default function () {
  const visitor: Visitor<{ filename: string; file: { code: string } }> = {
    Program(_, state) {
      const filename = state.filename;
      if (/node_modules/.test(filename)) {
        return;
      }
      reportFile(filename);
      const code = state.file.code;
      // 从代码中解析出依赖的文件，否则interface不能正确处理
      const results = code.matchAll(/from\s+['"](.*?)['"]/g);
      for (const [, module] of results) {
        resolveAndReport(state.filename, module);
      }
    },
    ImportDeclaration(p, state) {
      const source = p.node.source.value;
      resolveAndReport(state.filename, source);
    },
    CallExpression(p, state) {
      const callee = p.node.callee as any;
      const args = p.node.arguments as any;
      if (callee.name === 'require') {
        const source = args?.[0]?.value;
        if (source) {
          resolveAndReport(state.filename, source);
        }
      }
    },
  };
  return {
    name: 'find-useless-babel-plugin',
    visitor,
  };
}
