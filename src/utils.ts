import { execFileSync } from 'child_process';
import readdirp from 'readdirp';
import path from 'path';
import http from 'http';

export const getRepoFiles = async () => {
  let allFiles: string[] = [];
  try {
    const output = execFileSync('git', ['ls-tree', '-r', 'HEAD', '--name-only'], {
      encoding: 'utf-8',
    });
    allFiles = output.split('\n').map((file) => file.trim());
  } catch (err) {
    // fallback
    const entryInfos = await readdirp.promise('.', {
      directoryFilter: ['!.git', '!node_modules*'],
      type: 'files',
    });
    allFiles = entryInfos.map((entry) => entry.path.replace(/\\/g, ''));
  }
  return allFiles;
};

export const reportFile = (filename: string) => {
  if (/node_modules/.test(filename)) {
    return;
  }
  filename = path.relative(process.cwd(), filename).replace('\\', '/');
  const url = new URL(`http://localhost:${process.env.FIND_USELESS_PORT}?file=${encodeURIComponent(filename)}`);
  http.request(url, { method: 'get' }).end();
};

export const parseArgs = (args: string[]) => {
  console.log(args);
  return args.reduce((parsed, arg) => {
    const [key, value = ''] = arg.split('=');
    return {
      ...parsed,
      [key]: value.replace(/^["']|["']$/g, ''),
    };
  }, {});
};

export const injectBabelPlugin = (config: any) => {
  if (!process.env.FIND_USELESS) {
    return config;
  }
  return {
    ...config,
    plugins: ['@upace/find-useless/build/babel-plugin', ...(config.plugins || [])],
  };
};
