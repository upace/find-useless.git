#!/bin/env node

import server from './server';
import { getRepoFiles, parseArgs } from './utils';
import { exec } from 'child_process';

const filename = __filename;
const whenIndex = process.argv.lastIndexOf('when');
const filenameIndex = process.argv.lastIndexOf(filename);
const args = process.argv.slice(filenameIndex + 1, whenIndex);
const cmd = process.argv.slice(whenIndex + 1).join(' ');

Object.assign(process.env, parseArgs(args));
process.env.FIND_USELESS_PORT = '31713';
process.env.FIND_USELESS = 'TRUE';

async function run() {
  let allFiles = await getRepoFiles();
  const includes = process.env['--includes'];
  const excludes = process.env['--excludes'];
  if (includes) {
    const reg = new RegExp(includes);
    allFiles = allFiles.filter((file) => reg.test(file));
  }
  if (excludes) {
    const reg = new RegExp(excludes);
    allFiles = allFiles.filter((file) => !reg.test(file));
  }
  const fileSet = new Set(allFiles);

  server.on('report-file', (filename) => {
    console.log('依赖文件:', filename);
    fileSet.delete(filename);
  });

  await server.listen();

  const cp = exec(cmd, { env: process.env }, (err) => {
    if (err) {
      console.error(err);
      console.log('-------------- 出错了 -----------------');
    } else {
      console.log(fileSet);
      console.log(`-------------- 以上文件可能是无用文件 (${fileSet.size}) -----------------`);
    }
    process.exit();
  });
  cp.stdout?.pipe(process.stdout);
}

run();
