import fs from 'fs';
import path from 'path';

let alias: { [key: string]: string[] } = {};

try {
  const paths = eval(`(${fs.readFileSync('tsconfig.json', 'utf-8')})`).compilerOptions.paths as {
    [key: string]: string[];
  };
  Object.entries(paths).forEach(([key, aliasArr]) => {
    if (!key.replace('*', '')) {
      return;
    }
    alias[key.replace('*', '')] = aliasArr.map((a) => a.replace('*', '').trim());
  });
} catch (err) {
  // ignore
}

if (process.env['--alias']) {
  alias = eval(`(${process.env['--alias']})`);
}

// 检查文件是否存在，使用缓存加快速度
const existsCache: { [key: string]: boolean } = {};
function fileExists(filename: string) {
  if (existsCache[filename] === undefined) {
    try {
      existsCache[filename] = fs.statSync(filename).isFile();
    } catch (err) {
      existsCache[filename] = false;
    }
  }
  return existsCache[filename];
}

const exts = ['.ts', '.tsx', '.js', '.jsx', '.json'];

const resolveRelativePath = (filename: string, moduleName: string) => {
  const head = path.resolve(filename, moduleName);
  const files = ['']
    .concat(exts)
    .concat(exts.map((ext) => `/index${ext}`))
    .map((end) => `${head}${end}`);
  return files.find(fileExists) || false;
};

const resolveAlias = (moduleName: string) => {
  for (const key of Object.keys(alias)) {
    if (moduleName.startsWith(key)) {
      const aliasModule = './' + alias[key] + moduleName.slice(key.length);
      const fullpath = resolveRelativePath(process.cwd(), aliasModule);
      if (fullpath) {
        return fullpath;
      }
    }
  }
  return false;
};

export const resolve = (dirname: string, moduleName: string): string | false => {
  if (moduleName.startsWith('.')) {
    return resolveRelativePath(dirname, moduleName);
  }
  try {
    const fullpath = require.resolve(moduleName);
    return fullpath;
  } catch (err) {
    return resolveAlias(moduleName);
  }
};
